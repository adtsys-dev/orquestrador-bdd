# language: pt

Funcionalidade: Validar que nada ocorra utilizando a tecla tab 

Cenario 01
Esquema do cenário: Validar o uso da tecla tab
    Dado que estou logado no sistema Orquestrador 
    E já tenha um ambiente criado "Ambiente 1"
    Quando clino no ambiente "Ambiente 1"
    Então sou redirecionada pra a tela de edição de ambiente
    Quando adiciono o objeto <objeto>
    E preencho o campo <campo> do menu lateral com qualquer valor
    E preciono o tecla tab
    Então o foco deve retornar para o canvas
    
  Exemplos:
   | <Objeto>       | <Campo>      | 
   | Instancia      | Senha        |
   | Load Balancer  | Request Path | 
   | Banco de Dados | Senha        | 
