# language: pt

Funcionalidade: Implementar política de retenção
Eu, como cliente, desejo configurar o tempo de retenção para dos snapshots no cloud provider 
a fim de diminuir o custo mensal.

#1
Esquema do Cenario: Incluir um novo backup
    Dado que estou logado no Orquestrador na aba "POLÍTICAS DE AGENDAMENTO"
    Quando clico no botão "Adicionar política de agendamento"
    Então o sistema devera abrir um pop-up "Adicionar políca de agendamento" 
    Quando clico no combo "Política" 
    Então o sistema exibe as seguintes opções
        | Opções               |
        | Ligar instâncias     |
        | Desligar instâncias  |
        | Backup de instâncias |  
    E seleciono o opção "Backup de instâncias"
    E o sistema exibe os seguintes campos
        | CAMPOS             |
        | Política           |
        | Nome da política   |
        | Repetir a cada     |
        | Repetir / No dia   |
        | Horário            |
        | Retenção de backup |
    Quando preencho o campo "Política" com o seguinte valor "Backup de instâncias"
    E preencho o campo "Nome da política" com o seguinte valor "Backup teste"
    E preencho o campo "Repetir a cada" com o seguinte valor <BACKUP>
    E preencho o campo "Repetir" com o seguinte valor "S"
    E preencho o campo "Horário" com o seguinte valor "00:00"
    E preencho o campo "Retenção de backup" com o seguinte valor <DIAS>
    Então o sstema deverá retornar a seguinte mensagem de erro <MENSAGEM>
    Quando preencho o campo "Retenção de backup" com o seguinte valor <DIAS_CORRETOS>
    E clico no botão "CRIAR"
    Então o sistema devera criar a nova politica com sucesso 

  Exemplos:
    | BACKUP   | DIAS    | MENSAGEM             | DIAS_CORRETOS |
    | 3 dias   | 2 dias  | Valor minimo 3 dias  | 3 dias        |
    | 1 semana | 6 dias  | Valor minimo 7 dias  | 7 dias        |
    | 2 meses  | 46 dias | Valor minimo 60 dias | 60 dias       |