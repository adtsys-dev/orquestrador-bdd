# language: pt

Funcionalidade: Validar Serviço de autenticação SSO.

#1
Esquema do Cenário: Validar campos obrigatorios
    Dado que estou na area de login do sistema Orquestrador
    Quando não preencho nenhum campo
    Então o botão "Entrar" deve ficar desabilitado
    Quando não preencho o seguinte campo <campo>
    Então o botão "Entrar" deve ficar desabilitado
    Quando preencho o seguinte campo <campo>
    E apago o que foi preenchido
    Então o botão "Entrar" deve voltar para o status desabilitado
    E o campo <campo> deve exibir a seguinte mensagem "Campo obrigatório"
    
    Exemplos:
        | campo |
        | Email |
        | Senha |

#2
Esquema do Cenário: Validar acesso
    Dado que estou na area de login do sistema Orquestrador "https://auth.hom.orquestrador.adtsys.com.br"
    E tenha os seguinte usuario cadastrado na base de dados
        | ID | EMAIL                      | SENHA     |
        | 1  | orquestrador@adtsys.com.br | adtsys123 |
    Quando preencho o campo email com o seguinte email invalido "teste@"
    Então o sistema exibe a seguinte mensagem "Deve ser um e-mail válido" 
    Quando preencho os dados de login com o seguinte email <EMAIL>
    E com a seguinte senha <SENHA>
    E clico no botão "Entrar"
    Então o sistema exibe a seguinte mensagem "Usuário ou senha inválidos."
    Quando preencho os dados de login com o seguinte email "orquestrador@adtsys.com.br"
    E com a seguinte senha "adtsys123"
    E clico no botão "Entrar"
    Então o login é realizado com sucesso
    E devo ser redirecionada para a pagina de listagem de ambientes

  Exemplos:
    | EMAIL                      | SENHA           |
    | orquestrador@adtsys.com.br | 124567896532145 |
    | teste@adtsys.com.br        | 123456          | 

#3
Esquema do Cenario: Validar a redefinição de senha
    Dado que estou na area de login do sistema Orquestrador
    Quando clico no link "Esqueceu sua senha?"
    Então abrira uma tela com os seguintes campos
        | CAMPO |
        | email |    
    Quando preencho o campo "email" com o seguinte email "o@adtsys.com.br"
    E clico no botão "Enviar E-mail de resetar senha"
    Então o sistema exibe a seguinte mensagem "O e-mail informado não é válido!"
    Quando preencho o campo "email" com o seguinte email "orquestrador@adtsys.com.br"
    E clico no botão "Enviar E-mail de resetar senha"
    Então o sistema exibe a seguinte mensagem "E-mail de alteração de senha enviado com sucesso."
    E envia um email com o token de redefinição de senha
    Quando clico no link do email "Redefinir minha senha"
    Então sou redirecionada para a tela de redefinição de senha com os seguintes campos
        | CAMPO                |
        | Senha                |
        | Senha (Confirmação)  |
    E preencho o campo "Senha" com a seguinte senha <SENHA>
    E preencho o campo "Senha (Confirmação)" com a seguinte senha <CONFIRMAR_SENHA>
    E clico no botão "Redefinir Senha"
    Então o sistema exibe a seguinte mensagem <MENSAGEM>
    Quando clico no link "Redefinir minha senha" do email que já teve a senha redefinida 
    Então sou redirecionada para a tela de redefinição de senha 
    E preencho o campo "Senha" com a seguinte senha "12345"
    E preencho o campo "Senha (Confirmação)" com a seguinte senha "12345"
    E clico no botão "Redefinir Senha"
    Então o sistema exibe a seguinte mensagem "O token informado não é mais válido. Por favor solicite uma nova recuperação de senha."
    Quando tento logar com o seguinte email "orquestrador@adtsys.com.br"
    E com a seguinte senha antiga "adtsys123"
    Então o sistema deve exibir a seguinte mensagem "Email ou/e senha invalido(s)"
    Quando tento logar com o seguinte email "orquestrador@adtsys.com.br"
    E com a seguinte senha nova "12456"
    Então o login é realizado com sucesso
    
    Exemplos:
        | SENHA  | CONFIRMAR_SENHA | MENSAGEM                           |
        | 123456 | 111111          | Senhas não conferem!               |
        |        |                 | Campo obrigatório                  |
        | 123456 | 123456          | Atualização realizada com sucesso! |

#4
Esquema do Cenário: Verificar a utilização tecla tab 
    Dado que estou na area de login do sistema Orquestrador
    Quando clico no campo "E-mail"
    E preencho com a seguinte informação "orquestrador@adtsys.com.br"
    E aperto a tecla tab 
    Então o foco deve mudar para o campo senha 
    Quando preencho o campo senha com a seguinte informação "adtsys123" 
    E aperto a tecla tab duas vezes
    Então o foco deve mudar para o botão "Entrar"
    Quando aperto a techa "Enter"
    Então o sistema deverá realizar o login com sucesso 
    E devo ser redirecionada para a pagina de listagem de ambientes

#5
Esquema do Cenário: Validar acesso
    Dado que ejá tenha cadastrado o seguinte usuario
        | Usuario | E-mail          | Senha |
        | Teste   | teste@teste.com | 12345 |
    E que relacionado a esse usuario tenha os seguintes ambientes cadastrados: "AMBIENTE-1", "AMBIENTE-2", "AMBIENTE-3", "AMBIENTE-4" 
    E tenha a seguinte chave ssh cadastrada "chave ssh teste"
    E tenha as seguintes contas de provedores cadastradas na AWS, GOOGLE e AZURE respectivamente "AWS produto", "GCP Produtos" e "azure produtos"
    E tenha configurado os buckets das contas AWS e GOOGLE
    E tenha as seguintes políticas de agendamento cadastradas: "Backup teste", "Ligar teste","Desligar teste"
    E tenha os seguintes usuarios cadastrados "Orquestrador", "Teste"
    Quando preencho os dados de login com o seguinte email "teste@teste.com"
    E com a seguinte senha "12345"
    E clico no botão "Entrar"
    Então o login é realizado com sucesso
    E devo ser redirecionada para a pagina de listagem de ambientes contendo os seguintes ambientes: "AMBIENTE-1", "AMBIENTE-2", "AMBIENTE-3", "AMBIENTE-4"
    Quando clico na aba Faturamento
    Então deve carregar os dados com sucesso
    Quando clico na aba Configurações
    E na opção "Chaves SSH"
    Então deve carregar os dados com sucesso contendo "chave ssh teste"
    Quando clico na aba Configurações
    E na opção "Contas de Provedores"
    Então deve carregar os dados com sucesso contendo "AWS produto", "GCP Produtos" e "azure produtos"
    E os dados de bucket
    Quando clico na aba Configurações
    E na opção "Politica de Agendamento"
    Então deve carregar os dados com sucesso contendo "Backup teste", "Ligar teste","Desligar teste"
    Quando clico na aba Configurações
    E na opção "Usuario"
    Então deve carregar os dados com sucesso contendo "Orquestrador", "Teste"
    
#6
Cenário: Validar acesso de usuario recem criado
    Dado que estou logado no sistema Orquestrador com o usuario "orquestrador@adtsys.com.br"
    E estou na aba de usuarios "https://hom.orquestrador.adtsys.com.br/settings/users"
    Quando cadastro um novo usuario com o seguinte email "teste@mailinator.com"
    E com a seguinte senha "123456"
    Então o usuario deve ser cadastrado com sucesso 
    Quando realizo logout
    E preencho os dados de login com o seguinte email "teste@mailinator.com"
    E com a seguinte senha "123456"
    E clico no botão "Entrar"
    Então o login é realizado com sucesso
    E devo ser redirecionada para a pagina de listagem de ambientes
