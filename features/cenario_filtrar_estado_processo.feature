# language: pt

Funcionalidade: Eu como cliente, desejo ter filtros de reprovados 
e pendente de aprovação para que eu possa achar meus ambientes mais facilmente.

#1
Cenario: Validar filtro "Em aprovação" e o filtro "Reprovado"
  Dado que estou logado no orquestrador 
  E possua os seguintes ambientes com os seguintes estados de processo
    | Ambiente   | Estados de processo |
    | ambiente 1 | Reprovado           |
    | ambiente 2 | Em aprovação        |
    | ambiente 3 | Reprovado           |
    | ambiente 4 | Em aprovação        |
    | ambiente 5 | Reprovado           |
    | ambiente 6 | Em aprovação        |
    | ambiente 7 | Reprovado           |
    | ambiente 8 | Em aprovação        |
  Quando clico no seguinte filtro "Em aprovação"
  Então o filtro deve ficar selecionado 
  E exibir apenas os seguintes ambientes 
    | Ambientes  |
    | ambiente 2 |
    | ambiente 4 |
    | ambiente 6 |
    | ambiente 8 |
  Quando clico novamente no filtro "Em aprovação"
  Então o filtro deve voltar ao estado inicial
  E deve exibir todos os ambientes novamente
  Quando clico no seguinte filtro "Reprovado"
  Então o filtro deve ficar selecionado 
  E exibir apenas os seguintes ambientes 
    | Ambientes  |
    | ambiente 1 |
    | ambiente 3 |
    | ambiente 5 |
    | ambiente 7 |
  Quando clico novamente no filtro "Reprovado"
  Então o filtro deve voltar ao estado inicial
  E deve exibir todos os ambientes novamente

#2
Cenario: Validar a seleção dos dois filtros "Em aprovação" e "Reprovado"
  Dado que estou logado no orquestrador 
  E possua os seguintes ambientes com os seguintes estados de processo
    | Ambiente   | Estados de processo |
    | ambiente 1 | Reprovado           |
    | ambiente 2 | Em aprovação        |
    | ambiente 3 | Reprovado           |
    | ambiente 4 | Em aprovação        |
    | ambiente 5 | Reprovado           |
    | ambiente 6 | Em aprovação        |
    | ambiente 7 | Reprovado           |
    | ambiente 8 | Em aprovação        |
  Quando clico no seguinte filtro "Em aprovação" 
  E clico no seguinte filtro "Reprovado"
  Então os filtros devem ficar selecionados 
  E exibir apenas os seguintes ambientes 
    | Ambientes  |
    | ambiente 1 | 
    | ambiente 2 |
    | ambiente 3 |
    | ambiente 4 |
    | ambiente 5 | 
    | ambiente 6 | 
    | ambiente 7 |
    | ambiente 8 |
  Quando clico novamente no filtro "Em aprovação"
  Então o filtro deve voltar ao estado inicial
  E exibir apenas os seguintes ambientes 
    | Ambientes  |
    | ambiente 1 |
    | ambiente 3 |
    | ambiente 5 |
    | ambiente 7 |

#3
Esquema do Cenario: Validar os filtros de estados de processos e filtros de provedores
  Dado que estou logado no orquestrador 
  E possua os seguintes ambientes com os seguintes provedores
  E com os seguintes estados de processo 
    | Ambiente   | Estados de processo | Provedor |
    | ambiente 1 | Reprovado           | AWS      |
    | ambiente 2 | Em aprovação        | Google   |
    | ambiente 3 | Reprovado           | Azure    |
    | ambiente 4 | Em aprovação        | AWS      |
    | ambiente 5 | Reprovado           | Google   |
    | ambiente 6 | Em aprovação        | Azure    |
    | ambiente 7 | Reprovado           | AWS      |
    | ambiente 8 | Em aprovação        | Google   |
  Quando clico no seguinte filtro <estados_processos>
  E clico no seguinte filtro <provedor>
  Então os filtros devem ficar selecionados 
  E exibir apenas os seguintes ambientes <ambientes>

  Exemplos:
    | estados_processos | provedor | ambientes              |
    | Reprovado         | AWS      | ambiente 1, ambiente 7 |
    | Em aprovação      | AWS      | ambiente 4             |
    | Reprovado         | Google   | ambiente 5             |
    | Em aprovação      | Google   | ambiente 2, ambiente 8 |
    | Reprovado         | Azure    | ambiente 3             |
    | Em aprovação      | Azure    | ambiente 6             |

