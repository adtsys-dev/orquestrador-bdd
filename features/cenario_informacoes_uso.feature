# language: pt

Funcionalidade: Validar as informações de uso.

#1
Cenário: Validar informações de uso modo compartilhado na personalização da intancia
    Dado que estou logado no sistema Orquestrador
    Quando clico no botão "Criar ambiente"
    E preencho o campo "Nome do ambiente" com o seguinte nome "AMBIENTE 1"
    E clico no botão "Criar novo ambiente"
    Então sou redirecionada para o canvas
    Quando adiciono uma instancia
    E clico na opção do menu "Hardware"
    E clico na opção "Personalizar"
    Então é exibido o pop-up "Personalizar Hardware"
    E na opção "compartilhado?" é exibida a seguinte descrição "Descrição"
    
#2
Cenário: Validar informações de uso ao passar o mouse no icone "ip"
    Dado que estou logado no sistema Orquestrador
    Quando clico no botão "Criar ambiente"
    E preencho o campo "Nome do ambiente" com o seguinte nome "AMBIENTE 1"
    E clico no botão "Criar novo ambiente"
    Então sou redirecionada para o canvas
    Quando adiciono uma instancia
    E passo o mouse sobre o icone "ip" da instancia 
    Então é exibida a seguinte descrição "Descrição"

#3   
Cenário: Validar informações de uso período de retenção
    Dado que estou logado no sistema Orquestrador
    E clico no menu "Configurações"
    E clico na opção "Políticas de Agendamento"
    Quando clico no botão "Adicionar política de agendamento"
    E seleciono a opção "Backup de instâncias"
    E clico no help ao lado da label "Período de Retenção"
    Então é exibida a seguinte descrição "Descrição"
