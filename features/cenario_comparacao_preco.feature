 # language: pt
 
Funcionalidade: Comparar preços AWS e Google
Eu como usuário arquiteto, desejo simular um faturamento do Google à partir de um CSV de faturamento da AWS.

#1
Cenário: Validar aquivo csv invalido
    Dado que estou logado no sistema Orquestrador Arquitetura
    Quando clico na opção "Clique aqui"
    E seleciono um arquivo invalido no formato csv
    Então o sistema deverá fazer o upload 
    E exibir a seguinte mensagem "CSV inválido" 
    
#2
Cenário: Validar aquivo não no formato csv
    Dado que estou logado no sistema Orquestrador Arquitetura
    Quando clico na opção "Clique aqui"
    E seleciono um arquivo no formato diferente de csv
    Então o sistema deverá fazer o upload 
    E exibir a seguinte mensagem "CSV inválido" 
    
#3
Cenário: Validar aquivo csv valido 
    Dado que estou logado no sistema Orquestrador Arquitetura
    Quando clico na opção "Clique aqui"
    E seleciono um arquivo valido
    Então o sistema deverá fazer o upload 
    E o quadro de upload deverá desaparecer, exibindo a seguinte mensagem "Processando seu faturamento, isso pode demorar alguns poucos minutos"
    Quando o processo acabar o sistema devera exibir uma tabela com a comparação de preços da AWS e Google contendo as seguintes informações
        | Total de Instâncias            |
        | Total de bancos de dados       |
        | Total de Load balancers        |
        | Total de volumes               |
        | Valor total                    |
        | Valor do que não foi comparado |
    E deve exibir o mês e ano do csv processado 
    E um botão para download do csv deve ser exibido
    Quando clico no botão para realizar o download do csv
    Então o download deve ser feito
    E o csv deve conter informações detalhada de todos os itens comparados 
    
#4
Cenário: Comparar csv gerado com as infromações exibidas na tela
    Dado que estou logado no sistema Orquestrador Arquitetura
    E o sistema já tenha processado um arquivo csv valido
    Quando realizo o download do csv gerado com a comparação de preço 
    E comparo as informções do csv com as informações exibida na tela
    Então os valores devem ser identicos 
   
#5
Cenário: Validar seleção de arquivo 
    Dado que estou logado no sistema Orquestrador Arquitetura
    Quando clico na opção "Clique aqui"
    E seleciono um arquivo valido
    Então o sistema deverá fazer o upload do arquivo
    Quando arrasto um documento valido até a area indicada 
    E solto o documento
    Então o sistema deverá fazer o upload do arquivo
    
#6
Esquema do Cenário: Validar mes e ano exibidos na tela 
    Dado que possua um csv da seguinte data <data> 
    E estou logado no sistema Orquestrador Arquitetura
    Quando clico na opção "Clique aqui"
    E seleciono um arquivo valido
    Então o sistema deverá fazer o upload do arquivo
    E exibir a seguinte data <data>
    
    Exemplos: 
        | data    |
        | 09/2018 |
        | 05/2017 |
        | 08/2018 |

#7
Cenário: Validar o /help
    Dado que estou logado no sistema Orquestrador Arquitetura
    Quando clico na frase "Como obtenho o arquivo CSV de faturamento de minha conta AWS?"
    Então sou redirecionada para o /help com as informações para obter o csv de faturamento
