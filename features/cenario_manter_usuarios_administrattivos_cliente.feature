#language: pt

Funcionalidade: Manter usuários administrativos do cliente
Eu como canal, desejo criar e visualizar usuários do cliente para que múltiplos usuários dos meus clientes possam acessar o cloudHit.

# Cenário 01
    Cenário: Validar obrigatoriedade dos campos para Inclusão de Clientes
        Dado que estou logado no Orquestrador com o perfil canal
        E acesso a área administrativa do canal
        E não informo o campo "Nome"
        E informo o campo "Email"
        Quando clico no botão "Salvar"
        Então o sistema exibe a seguinte mensagem "Campo Obrigatório"
        Quando informo o campo "Nome"
        E não informo o campo "Email"
        Quando clico no botão "Salvar"
        Então o sistema exibe a seguinte mensagem "Campo obrigatório"
        Quando informo mais que 255 caracteres para o campo "Nome"
        Então o sistema nâo permite digitar mais que 255 caracteres
        Quando informo mais que 60 caracteres para o campo "Email"
        Então o sistema não permite informar mais que 60 caracterespara
        Quando o usuário informa o seguinte email inválido "luciano@teste"
        E clico no botão "Salvar"
        Então o sistema exibe mensagem "Email inválido"

        
# Cenário 02
    Cenário: Validar a funcionalidade de inclusão
        Dado que estou logado no Orquestrador com o perfil canal
        E acesso a área administrativa do canal
        E informo os seguintes campos
         |campos|Valor                      |
         |Nome  |Cauã Thomas Murilo Lima    |
         |Email |luciano@teste.com.br       |
        Quando clico no botão "Salvar"
        Então o sistema dispara email quando o usuário é criado
        E a senha provisória é gerada automaticamente
        E é possivel logar na url "https://hom.orquestrador.adtsys.com.br/" com o email "luciano@teste.com.br" e com a senha enviada no email

#3
Cenario: Incluir um cliente e verificar email 
    Dado que estou logado na área administrativa do canal
    E os seguintes dados existentes na tabela "TENANTS"
        | ID | NOME                            | EMAIL                          | 
        | 1  | Larissa e Mariana Eletrônica ME | larissa.espinosa@adtsys.com.br | 
    Quando clico no botão "Novo Cliente" 
    E preencho o campo "Nome" com o seguinte valor "Larissa Sitta Espinosa"
    E preencho o campo "Email" com o seguinte valor "larissa.espinosa@adtsys.com.br"
    E clico no botão "Salvar"
    Quando eu atualizo o campo <CAMPO> de <VALOR_ANTERIOR> para <NOVO_VALOR>
    E clico no botão "Salvar"
    Então o sistema deve exibir a seguinte mensagem de erro "Email já em uso"
    
#4
Cenario: Visualizar informações do cliente
    Dado que estou logado na área administrativa do canal
    E tenha o seguinte cliente cadastrado
        | ID | NOME                   | EMAIL                          | 
        | 1  | Larissa Sitta Espinosa | larissa.espinosa@adtsys.com.br | 
    Quando clico no link "Visualizar" do cliente "Larissa Sitta Espinosa" 
    Então o sistema deve abrir uma tela contendo as informações do cliente com os seguintes campos
        | Campos |
        | Nome   |
        | Email  |
    E os respequitivos valores
        | Valores                        |
        | Larissa Sitta Espinosa         |
        | larissa.espinosa@adtsys.com.br |