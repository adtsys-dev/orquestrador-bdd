# language: pt

Funcionalidade: Validar os emais listados para aprovação e reprovação de ambientes

Dado os seguintes dados existentes na tabela "Usuarios":
      | Email                          | Permissao          | 
      | orquestrador@adtsys.com.br     | Aprovador          |
      | larissa.espinosa@adtsys.com.br |                    |
      | aprovador@teste.com            | Aprovador          |
      | usuario@teste.com.br           | Gerenciar Usuários |
     
Cenário 01
Esquema do cenário: Cadastrar usuários com e sem permissões de gerenciamento de usuarios, lançamento de ambientes e aprovador 
    Dado que estou logado no sistema Orquestrador com perfil "Gerenciamento de usuários"
    E seleciono a funcionalidade "Adicionar usuário" 
    E informo os seguintes campos obrigatórios para a tela "Adicionar usuário"
     | Campos            |
     | <Nome>            |
     | <E-mail>          |
     | <Senha>           |
     | <Confirmar senha> |
     | <Permissão>       |
    Quando clico no botão "Criar"
    Então o usuário é criado com sucesso

 Exemplos:
       | <Nome>  | <E-mail>         | <Senha> | <Confirmar senha> | <Permissão>        |
       | Teste 1 | teste@teste.com  | 123456  | 123456            | Gerenciar Usuários |
       | Teste 1 | teste@teste.com  | 123456  | 123456            | Lançar Ambientes   |
       | Teste 1 | teste@teste.com  | 123456  | 123456            | Aprovador          |
       | Teste 2 | teste2@teste.com | 123456  | 123456            | Aprovador          |
       | Teste 3 | teste3@teste.com | 123456  | 123456            |                    |

Cenário 02 
 Esquema do cenário: Validar os emails listados para aprovação e reprovação de ambientes
    Dado que estou logado no Orquestrador com o usuario "usuario@teste.com.br" 
    E clico na funcionalidade "Ambientes"
    E clico na funcionalidade "Criar Ambiente"
    E preencho os campos obrigatórios
    E clico no botão "Criar novo ambiente"
    E seleciono a funcionalidade "Selecione um provedor"
    E seleciono o <Provedor>
    Quando clico em "CONFIRMAR"
    Então o botão "ENVIAR PARA APROVAÇÃO" deve estar habilitado
    Quando clico no botão "ENVIAR PARA APROVAÇÃO"
    Então o sistema exibe o pop-up "Enviar para Aprovação" 
    E o campo "Email do usuário aprovador" deve listar os seguintes emails <E-mail>
    Quando seleciono um email <E-mail>
    E informo qualquer texto para o campo "Descrição"
    E clico no botão "Enviar"
    Então o sistema retorna para a tela inicial "Ambientes" 
    E exibe uma tag "EM APROVAÇÃO"
    E dispara um email para o aprovador <E-mail>

    Exemplos:
   | <E-mail>                   | <Provedor>   | 
   | orquestrador@adtsys.com.br | AWS          |
   | aprovador@teste.com        | AWS          |
   | orquestrador@adtsys.com.br | Google Cloud |
   | aprovador@teste.com        | Google Cloud |
   | orquestrador@adtsys.com.br | Azure        |
   | aprovador@teste.com        | Azure        |
 
 Cenário 03 
 Esquema do cenário: Adicionar permissao aprovador e validar os emails listados para aprovação e reprovação de ambientes
    Dado que estou logado no Orquestrador com o usuario "usuario@teste.com.br" 
    E clico na funcionalidade "Usuarios"
    Quando seleciono o usuario "larissa.espinosa@adtsys.com.br"
    E adiciono a permissão "Aprovador"
    E clico no botão "Atualizar"
    Então os dados devem ser salvos com sucesso
    Quando clico na funcionalidade "Ambientes"
    E clico na funcionalidade "Criar Ambiente"
    E preencho os campos obrigatórios
    E clico no botão "Criar novo ambiente"
    E seleciono a funcionalidade "Selecione um provedor"
    E seleciono o qualquer um dos provedores
    Quando clico em "CONFIRMAR"
    Então o botão "ENVIAR PARA APROVAÇÃO" deve estar habilitado
    Quando clico no botão "ENVIAR PARA APROVAÇÃO"
    Então o sistema exibe o pop-up "Enviar para Aprovação" 
    E o campo "Email do usuário aprovador" deve listar os seguintes emails <E-mail>

    Exemplos:
   | <E-mail>                       | 
   | orquestrador@adtsys.com.br     |
   | aprovador@teste.com            |
   | larissa.espinosa@adtsys.com.br |

Cenário 04 
 Esquema do cenário: Retirar permissao aprovador e validar os emails listados para aprovação e reprovação de ambientes
    Dado que estou logado no Orquestrador com o usuario "usuario@teste.com.br" 
    E clico na funcionalidade "Usuarios"
    Quando seleciono o usuario "aprovador@teste.com "
    E retirar a permissão "Aprovador"
    E clico no botão "Atualizar"
    Então os dados devem ser salvos com sucesso
    Quando clico na funcionalidade "Ambientes"
    E clico na funcionalidade "Criar Ambiente"
    E preencho os campos obrigatórios
    E clico no botão "Criar novo ambiente"
    E seleciono a funcionalidade "Selecione um provedor"
    E seleciono o qualquer um dos provedores
    Quando clico em "CONFIRMAR"
    Então o botão "ENVIAR PARA APROVAÇÃO" deve estar habilitado
    Quando clico no botão "ENVIAR PARA APROVAÇÃO"
    Então o sistema exibe o pop-up "Enviar para Aprovação" 
    E o campo "Email do usuário aprovador" deve listar os seguintes emails <E-mail>

    Exemplos:
   | <E-mail>                       | 
   | orquestrador@adtsys.com.br     |
  