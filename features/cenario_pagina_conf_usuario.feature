# language: pt

Funcionalidade: Eu como administrador do sistema, desejo que seja criada uma página de configurações no CloudHit para que itens do menu como chaves SSH e usuários.

#1
Cenário: Validar as opções exibidas no cabeçalho
    Dado que estou logado no sistema Orquestrador 
    Então as opções exibidas no cabeçalho deverão ser as seguintes
        | Opções        |
        | Ambientes     |
        | Faturamento   |
        | Configurações |

#2
Cenário: Validar as opções exibidas na opção "Configurações"
    Dado que estou logado no sistema Orquestrador 
    E cliclo na opção "Configurações"
    Então as opções exibidas deverão ser as seguintes
        | Opções                  |
        | Chaves SSH              |
        | Contas de Provedores    |
        | Politica de Agendamento |
        | Usuários                |
    E as opções devem estar em ordem alfabetica 

#3    
Esquema do Cenário: Validar a url na opção "Configurações"
    Dado que estou logado no sistema Orquestrador 
    E cliclo na opção "Configurações"
    Quando clico na opção <opcao>
    Então sou redirecionada para a url <url> 
 
 Exemplos:
    | opcao                   | url                         |
    | Chaves SSH              | /settings/provider-accounts |
    | Contas de Provedores    | /settings/ssh-keys          |
    | Politica de Agendamento | /settings/schedule-policies |
    | Usuários                | /settings/users             |

#4
Cenário: Validar que usuario que não tenha a permissão "Gerenciar usuarios" veja a opção "Usuarios"
    Dado que estou logado no sistema Orquestrador com um usuario que não tenha a permissão "Gerenciar Usuario"
    E cliclo na opção "Configurações"
    Então as opções exibidas deverão ser as seguintes
        | Opções                  |
        | Chaves SSH              |
        | Contas de Provedores    |
        | Politica de Agendamento |
    E as opções devem estar em ordem alfabetica 
    E a opção "Usuários" não deve ser exibida