# language: pt

Funcionalidade: Manter usuários administrativos do canal
Eu como canal, desejo manter meus usuários para que múltiplas pessoas da minha empresa possam administrar a plataforma administrativa.