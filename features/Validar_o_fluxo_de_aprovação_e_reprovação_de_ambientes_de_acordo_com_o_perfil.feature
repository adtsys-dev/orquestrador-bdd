# language: pt

Funcionalidade: Validar o fluxo de aprovação e reprovação de ambientes de acordo com o perfil

Contexto:  
Esquema do Cenário: Cadastrar usuários com e sem permissões de gerenciamento de usuarios e lançamento de ambientes 
    Dado que estou logado no sistema Orquestrador com perfil "Gerenciamento de usuários"
    E seleciono a funcionalidade "Adicionar usuário" 
    E informo os seguintes campos obrigatórios para a tela "Adicionar usuário"
     |Campos            |
     |<Nome>            |
     |<E-mail>          |
     |<Senha>           |
     |<Confirmar senha> |
     |<Permissão>       |
    Quando clico no botão "Criar"
    Então o usuário é criado com sucesso
  
  Exemplos:
   | <Nome>  | <E-mail>                          | <Senha> | <Confirmar senha> | <Permissão>        |
   | Larissa | larissa.espinosa@adtsys.com.br    | 123456  | 123456            | Gerenciar Usuários |
   | Larissa | larissa.espinosa@adtsys.com.br    | 123456  | 123456            | Lançar Ambientes   |
   | Alana   | larissasitta.espinosa@gmail.com   | 123456  | 123456            | Gerenciar Usuários |
   | Nathan  | larissasitta.espinosa@hotmail.com | 123456  | 123456            | Lançar Ambientes   |
   | Luciano | luciano.ribeiro@adtsys.com.br     | 123456  | 123456            |                    |

 # Cenário 01 
  Esquema do Cenário: Validar permissão de lançamento de ambiente para o perfil "Lançar Ambientes"
    Dado que estou logado no Orquestrador com o <E-mail> 
    E clico na funcionalidade "Ambientes"
    E clico na funcionalidade "Criar Ambiente"
    E preencho os campos obrigatórios
    E clico no botão "Criar novo ambiente"
    E seleciono a funcionalidade "Selecione um provedor"
    E seleciono o <Provedor>
    Quando clico em "CONFIRMAR"
    Então o botão "LANÇAR" deve estar habilitado

    Exemplos:
   | <E-mail>                          | <Provedor>   | 
   | larissa.espinosa@adtsys.com.br    | AWS          |
   | larissasitta.espinosa@hotmail.com | AWS          |
   | larissa.espinosa@adtsys.com.br    | Google Cloud |
   | larissasitta.espinosa@hotmail.com | Google Cloud |
   | larissa.espinosa@adtsys.com.br    | Azure        |
   | larissasitta.espinosa@hotmail.com | Azure        |

# Cenário 02 
  Esquema do Cenário: Validar o workflow de lançamento de ambiente por perfil sem permissão "Lançar Ambientes"
    Dado que estou logado no Orquestrador com o <E-mail> 
    E clico na funcionalidade "Ambientes"
    E clico na funcionalidade "Criar Ambiente"
    E preencho os campos obrigatórios
    E clico no botão "Criar novo ambiente"
    E seleciono a funcionalidade "Selecione um provedor"
    E seleciono o <Provedor>
    Quando clico em "CONFIRMAR"
    Então o botão "ENVIAR PARA APROVAÇÃO" deve estar habilitado
    Quando clico no botão "ENVIAR PARA APROVAÇÃO"
    Então o sistema exibe o pop-up "Enviar para Aprovação" 
    Quando informo o seguinte formato invalido "teste@teste" para o campo "Email do usuário aprovador"
    E clico no botão "Enviar"
    Então o sistema deve exibir uma mensagem de validação para o "Email" invalido
    Quando informo o seguinte email valido "larissa.espinosa@adtsys.com.br" para o campo "Email do usuário aprovador"
    E informo qualquer texto para o campo "Descrição"
    Quando clico no botão "Enviar"
    Então o sistema retorna para a tela inicial "Ambientes" 
    E exibe uma tag "EM APROVAÇÃO"
    E dispara um email para o aprovador "larissa.espinosa@adtsys.com.br"

    Exemplos:
   | <E-mail>                        | <Provedor>   | 
   | luciano.ribeiro@adtsys.com.br   | AWS          |
   | larissasitta.espinosa@gmail.com | AWS          |
   | luciano.ribeiro@adtsys.com.br   | Google Cloud |
   | larissasitta.espinosa@gmail.com | Google Cloud |
   | luciano.ribeiro@adtsys.com.br   | Azure        |
   | larissasitta.espinosa@gmail.com | Azure        |

# Cenário 03
    Cenário: Validar template de email 
        Dado que o perfil "Lançar Ambientes" recebeu a solicitação de aprovação por email
        Então o template de email devera exibir os seguintes campos
         | Campos                                |
         | Nome do Ambiente                      |
         | Link para acessar o Orquestrador      |
         | Nome do Requisitante                  |
         | Provedor escolhido                    |
         | Valor total do lançamento do ambiente |
         | Configuração do ambiente              |
         | Descrição                             |
         | Ação                                  |
        E os seguintes botões
         | Botões   |
         | Aprovar  |
         | Reprovar |
         | Submeter |
        
# Cenário 04
    Cenário: Validar a reprovação do ambiente por email
        Dado que o perfil "Lançar Ambientes" recebeu a solicitação de aprovação por email
        Quando clico no botão "Reprovar"
        Então o sistema deve exibir o campo "Comentário" como não obrigatório
        Quando informado um Comentário
        E clicar no botão "submeter"
        Então o sistema devera dispara um email para o requisitante
        E informa que o ambiente foi reprovado contendo ou não o motivo da reprovação 
        E o token do email é expirado
        Quando o usuário tentar acessar pelo token expirado
        Então o sistema devera exibir na tela "Token Expirado"
        Quando o requisitante posicionar o mouse sobre o icone presente na tela "Ambientes"
        Então o tooltip exibirá o motivo da reprovação 
        E o ambiente ficará com uma tag "Reprovado"

# Cenário 05
    Cenário: Validar a aprovação do ambiente por email
        Dado que o perfil "Lançar Ambientes" recebeu a solicitação de aprovação por email
        Quando clico no botão "Aprovar"
        E clicar no botão "submeter"
        Então o sistema devera dispara um email para o requisitante informando a aprovação do ambiente
        E o token do email é expirado
        E lançara o ambiente no provedor escolhido 

# Cenário 06
    Cenário: Validar a edição após o ambiente já estar reprovado
        Dado que estou logado no Orquestrador com o perfil sem permissão "Lançar Ambiente"
        E seleciono um ambiente reprovado para edita-lo
        E realizo as alterações necessarias 
        Quando seleciono o botão "ENVIAR PARA APROVAÇÃO"
        Então o sistema deve exibir o pop-up "Enviar para Aprovação" 
        Quando informo o seguinte email valido "larissa.espinosa@adtsys.com.br" para o campo "Email do usuário aprovador"
        E informo qualquer texto para o campo "Descrição"
        Quando clico no botão "Enviar"
        Então o sistema retorna para a tela inicial "Ambientes" 
        E altera a tag de "REPROVADO" para "EM APROVAÇÃO"
        E dispara um email para o aprovador "larissa.espinosa@adtsys.com.br"
        E invalida o token do email anterior 
        Quando o usuário tentar acessar pelo token expirado
        Então o sistema devera exibir na tela "Token Expirado"

# Cenário 07
    Cenário: Validar a edição quando o ambiente está em processo de aprovação que ainda não foi lançado
        Dado que estou logado no Orquestrador com o perfil sem permissão "Lançar Ambiente"
        E seleciono um ambiente não lançado com tag "EM APROVAÇÃO"
        E realizo as alterações necessarias 
        E seleciono o botão "ENVIAR PARA APROVAÇÃO"
        Então exibir o pop-up "Enviar para Aprovação" 
        Quando informo o seguinte email valido "larissa.espinosa@adtsys.com.br" para o campo "Email do usuário aprovador"
        E informo qualquer texto para o campo "Descrição"
        Quando clico no botão "Enviar"
        Então o sistema retorna para a tela inicial "Ambientes" 
        E exibe uma tag "EM APROVAÇÃO"
        E dispara novamente um email para o aprovador "larissa.espinosa@adtsys.com.br"
        E invalida o token do email anterior 
        Quando o usuário tentar acessar pelo token expirado
        Então o sistema devera exibir na tela "Token Expirado"

# Cenário 08 
    Cenário: Validar o menu "USUÁRIOS" para o perfil sem permissão "Gerenciar Usuários"
        Dado que estou logado no Orquestrador com o email luciano.ribeiro@adtsys.com.br
        Então o sistema não deverá exibir a aba "USUÁRIOS"