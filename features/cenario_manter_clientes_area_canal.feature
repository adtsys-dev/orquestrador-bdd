# language: pt

Funcionalidade: Manter clientes na área do canal
Eu como canal, desejo manter meus clientes (tenants) para gerenciar os acessos ao cloudHit.

https://docs.google.com/document/d/1stWYtZ5WrYeEjYsgFcHQAV21Pq4lDbPFUHRsK7nXR70/edit#

#1
 Esquema do Cenario: Incluir cliente pela área administrativa do canal
    Dado que estou logado na área administrativa do canal
    E desejo inclur um novo cliente
    Quando clico no botão "Adicionar Novo Cliente"
    Então o sistema redirecionar para a tela de "Adicionar novo cliente" com os campos
        | Campos   |
        | Nome     |
        | CPF/CNPJ | 
    Quando não preencho um Campo obrigatorio
    E clico no botão salvar
    Então os sistema devera exibir a seguinte mensagem "Campo obrigatório"
    Quando preencho o campo <CAMPO>  com o seguinte valor invalido: <VALOR>
    Então o sistemaa devera exibir a seguinte mensagem <MENSAGEM>
    Quando preencho o campo "Nome" com o seguinte valor: "Larissa Sitta Espinosa" 
    E preencho o campo "CPF / CNPJ" com o seguinte valor: "351.010.428-54"
    E clico no botão "Salvar"
    Então o sistema exibe a seguinte mensagem "Cadastro criado com sucesso"

    Exemplos:
        | <CAMPO>  | <VALOR>            | <MENSAGEM>    |
        | CPF      | 136.895.758-62     | CPF inválido  |
        | CNPJ     | 00.000.000/0000-00 | CNPJ inválido |

#2
Cenario: Listar clientes(tenants) pela área administrativa do canal
    Dado que estou logado na área administrativa do canal
    E desejo visualizar os clientes(tenants) cadastrado(s)
    Então os clientes(tenants) cadastrado(s) é(são) listado(s), incluindo o(s) com status "inativo"

#3
Esquema do Cenario: Atualizar um cliente 
    Dado que estou logado na área administrativa do canal
    E os seguintes dados existentes na tabela "TENANTS"
        | ID | NOME                            | DOCUMENTO          | 
        | 1  | Larissa e Mariana Eletrônica ME | 19.111.127/0001-06 | 
        | 2  | Larissa Sitta Espinosa          | 351.010.428-54     | 
    Quando clico no botão "Editar" do cliente "Larissa e Mariana Eletrônica ME"
    Então o sistema devera redirecionar para a tela de "Atualizar Cliente(Tenants)" com os seguintes campos
        | Campos       |
        | Razão Social |
        | CPF/CNPJ     |
    Quando apago um dos campos obrigatórios
    Então o sistema devera exibir a seguinte mensagem "Campo obrigatório"
    Quando eu atualizo o campo <CAMPO> de <VALOR_ANTERIOR> para <NOVO_VALOR>
    E clico no botão "Salvar"
    Então o sistema deve persistir a alteração
    E exibir a seguinte mensagem "Atualização realizada com sucesso"

    Exemplos:
       | <CAMPO>      | <VALOR_ANTERIOR>                  | <NOVO_VALOR>                     |
       | Razão Social | Larissa e Mariana Eletrônica ME   | Marina e Larissa Informática ME  |
       | CPF/CNPJ     | 19.111.127/0001-06                | 27.593.329/0001-60               |

#4
Esquema do Cenario: Filtar resultados da listagem
    Dado que estou logado na área administrativa do canal
    E já tenha um ou mais cliente cadastrados
    Quando digito a seguinte informação <INFORMACAO> no seguinte <CAMPO>
    E clico no botão "Pesquisar"
    Então o sistema devera trazer os dados correspondentes
    Exemplos:
        | <CAMPO>   | <INFORMACAO> |
        | Nome      | Larissa      |
        | CPF/CNPJ  | 01           |

#5
Cenario: Desativar um cliente pela área administrativa do canal
    Dado que estou logado na área administrativa do canal
    E já tenha um ou mais clientes cadastrados e ativos
    Quando clico no link "Desativar" do clienre "Larissa e Mariana Eletrônica ME"
    Então o sistema deve exibir uma pop-up com a seguinte mensagem "Você tem certeza que deseja desativar esse registro?"
    Quando clico no botão "NÃO"
    Então o sistema deve manter o cliente ativo
    Quando clico no botão "SIM"
    Então o sistema devera alterar o status do cliente para inativo

#6
Cenario: Ativar um cliente pela área administrativa do canal
    Dado que estou logado na área administrativa do canal
    E já tenha um ou mais clientes cadastrados e inativos
    Quando clico no check "Ativar" do cliente "Larissa e Mariana Eletrônica ME"
    Então o sistema deve exibir uma pop-up com a seguinte mensagem "Você tem certeza que deseja ativar esse registro?"
    Quando clico no botão "NÃO"
    Então o sistema deve manter o cliente inativo
    Quando clico no botão "SIM"
    Então o sistema devera alterar o status do cliente para ativo

#7
Cenario: Um canal não pode ver o cliente do outro
    Dado que estou logado com o canal "Marina e Larissa Informática ME" na área administrativa do canal
    E os seguintes dados existentes na tabela "TENANTS"
        | ID | NOME                            | DOCUMENTO          | 
        | 1  | Larissa Sitta Espinosa          | 351.010.428-54     | 
    E os seguintes dados existentes na tabela "CANAIS"
        | ID | RAZÃO SOCIAL                    | NOME FANTASIA                   | CNPJ               | EMAIL ADMINISTRADOR               | NOME                   | TELEFONE        | EMAIL                             | RESPONSABILIDADE                                   |
        | 1  | Larissa e Mariana Eletrônica ME | Larissa e Mariana Eletrônica ME | 19.111.127/0001-06 | larissa.espinosa@adtsys.com.br    | Larissa Sitta Espinosa | (19) 99786-8781 | larissa.espinosa@adtsys.com.br    | Responsável pela venda do Orquestrador em Uberaba  |
        | 2  | Marina e Larissa Informática ME | Marina e Larissa Informática ME | 27.593.329/0001-60 | larissasitta.espinosa@hotmail.com | Larissa Espinosa       | (19) 99773-1976 | larissasitta.espinosa@hotmail.com | Responsável pela venda do Orquestrador em Pedreira |
    E o tenants "Larissa Sitta Espinosa" pertence ao canal "Larissa e Mariana Eletrônica ME"
    Então não visualizo clientes
    Quando altero o prfil de canal para "Larissa e Mariana Eletrônica ME"
    Então visualizo o cliente "Larissa Sitta Espinosa"